package com.goocadi.vehiclemodel.api;

import org.springframework.cloud.netflix.feign.FeignClient;
//import io.swagger.configuration.ClientConfiguration;

@FeignClient(name="${com.goocadi.client.vehiclemodel.name:com.goocadi.client.vehiclemodel}",
url="${com.goocadi.client.vehiclemodel.url:https://dev.goocadi.de}")
public interface VehiclemodelControllerApiClient extends VehiclemodelControllerApi {
}