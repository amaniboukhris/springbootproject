package com.goocadi.vehiclemodel.controller;

import com.goocadi.vehiclemodel.api.VehiclemodelControllerApi;
import com.goocadi.vehiclemodel.model.VehicleModel;
import com.goocadi.vehiclemodel.model.VehicleModelList;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Controller
@Slf4j
public class VehicleModelController implements VehiclemodelControllerApi {


    @Override
    public ResponseEntity<VehicleModel> getVehicleModel(@PathVariable("vehicleModelId") UUID vehicleModelId) {
        log.info("get vehicle with id {}", vehicleModelId);
        VehicleModel vehicleModel = new VehicleModel()
                .id(vehicleModelId)
                .brand("VW")
                .model("GOLF")
                .fuel(VehicleModel.FuelEnum.PETROL)
                .torNumber("4/5");
        log.info("found vehicle model: {}", vehicleModel);
        return ResponseEntity.ok(vehicleModel);
    }

    @Override
    public ResponseEntity<VehicleModelList> getVehicleModels(
            @PathVariable("brand") String brand,
            @PathVariable("model") String model,
            @PathVariable("constructionYear") String constructionYear,
            @RequestParam(value = "fuelType", required = false) String fuelType,
            @RequestParam(value = "torNumber", required = false) String torNumber) {
        log.info("get vehicles of brand {}", brand);
        VehicleModelList vehicleModelList = new VehicleModelList();
        vehicleModelList.add(new VehicleModel().brand(brand).model(model));
        return ResponseEntity.ok(vehicleModelList);
    }
}
