package com.goocadi.bff.api;

import org.springframework.cloud.netflix.feign.FeignClient;
//import io.swagger.configuration.ClientConfiguration;

@FeignClient(name="${com.goocadi.client.bff.name:com.goocadi.client.bff}",
url="${com.goocadi.client.bff.url:https://dev.goocadi.de}")
public interface BffVehicleModelControllerApiClient extends BffVehicleModelControllerApi {
}