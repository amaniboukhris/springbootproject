package com.goocadi.bff.controller;

import com.goocadi.bff.api.BffVehicleModelControllerApi;
import com.goocadi.vehiclemodel.api.VehiclemodelControllerApiClient;
import com.goocadi.vehiclemodel.model.VehicleModel;
import com.goocadi.vehiclemodel.model.VehicleModelList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Controller
@Slf4j
public class VehicleModelController implements BffVehicleModelControllerApi {

    @Autowired
    VehiclemodelControllerApiClient vehicleModelClient;

    @Override
    public ResponseEntity<VehicleModel> getVehicleModel(@PathVariable("vehicleModelId") UUID vehicleModelId) {
        log.info("get vehicle model from bff with id {}", vehicleModelId);
        ResponseEntity<VehicleModel> vehicleModelResponse = vehicleModelClient.getVehicleModel(vehicleModelId);
        VehicleModel vehicleModel = vehicleModelResponse.getBody();
        log.info("found vehicle model from the vehicle-model-service: {}", vehicleModel);
        if (vehicleModel.getBrand().equals("VW")){
            vehicleModel.brand("Volkswagen");
        }
        log.info("return the vehicle model: {}", vehicleModel);
        return vehicleModelResponse;
    }

    @Override
    public ResponseEntity<VehicleModelList> getVehicleModels(
            @PathVariable("brand") String brand,
            @PathVariable("model") String model,
            @PathVariable("constructionYear") String constructionYear,
            @RequestParam(value = "fuelType", required = false) String fuelType,
            @RequestParam(value = "torNumber", required = false) String torNumber) {
        return vehicleModelClient.getVehicleModels(brand, model, constructionYear, fuelType, torNumber);
    }
}
