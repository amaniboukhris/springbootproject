package com.goocadi.vehiclemodel.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Vehicle Model containing the principal construction characteristics
 */
@ApiModel(description = "Vehicle Model containing the principal construction characteristics")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-02-24T21:52:58.745+01:00")

public class VehicleModel   {
  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("brand")
  private String brand = null;

  @JsonProperty("model")
  private String model = null;

  @JsonProperty("torNumber")
  private String torNumber = null;

  @JsonProperty("startConstructionDate")
  private OffsetDateTime startConstructionDate = null;

  @JsonProperty("endConstructionDate")
  private OffsetDateTime endConstructionDate = null;

  /**
   * the fuel of the vehicle model
   */
  public enum FuelEnum {
    DIESEL("DIESEL"),
    
    PETROL("PETROL"),
    
    OTHER("OTHER");

    private String value;

    FuelEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static FuelEnum fromValue(String text) {
      for (FuelEnum b : FuelEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("fuel")
  private FuelEnum fuel = null;

  public VehicleModel id(UUID id) {
    this.id = id;
    return this;
  }

   /**
   * UUID of the vehicle model
   * @return id
  **/
  @ApiModelProperty(required = true, value = "UUID of the vehicle model")
  @NotNull

  @Valid

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public VehicleModel brand(String brand) {
    this.brand = brand;
    return this;
  }

   /**
   * the brand of the vehicle
   * @return brand
  **/
  @ApiModelProperty(required = true, value = "the brand of the vehicle")
  @NotNull


  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public VehicleModel model(String model) {
    this.model = model;
    return this;
  }

   /**
   * the model of the vehicle
   * @return model
  **/
  @ApiModelProperty(required = true, value = "the model of the vehicle")
  @NotNull


  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public VehicleModel torNumber(String torNumber) {
    this.torNumber = torNumber;
    return this;
  }

   /**
   * the tor number of the vehicle
   * @return torNumber
  **/
  @ApiModelProperty(required = true, value = "the tor number of the vehicle")
  @NotNull


  public String getTorNumber() {
    return torNumber;
  }

  public void setTorNumber(String torNumber) {
    this.torNumber = torNumber;
  }

  public VehicleModel startConstructionDate(OffsetDateTime startConstructionDate) {
    this.startConstructionDate = startConstructionDate;
    return this;
  }

   /**
   * Timestamp, when the construction of the vehicle model started
   * @return startConstructionDate
  **/
  @ApiModelProperty(required = true, value = "Timestamp, when the construction of the vehicle model started")
  @NotNull

  @Valid

  public OffsetDateTime getStartConstructionDate() {
    return startConstructionDate;
  }

  public void setStartConstructionDate(OffsetDateTime startConstructionDate) {
    this.startConstructionDate = startConstructionDate;
  }

  public VehicleModel endConstructionDate(OffsetDateTime endConstructionDate) {
    this.endConstructionDate = endConstructionDate;
    return this;
  }

   /**
   * Timestamp, when the construction of the vehicle model ended
   * @return endConstructionDate
  **/
  @ApiModelProperty(value = "Timestamp, when the construction of the vehicle model ended")

  @Valid

  public OffsetDateTime getEndConstructionDate() {
    return endConstructionDate;
  }

  public void setEndConstructionDate(OffsetDateTime endConstructionDate) {
    this.endConstructionDate = endConstructionDate;
  }

  public VehicleModel fuel(FuelEnum fuel) {
    this.fuel = fuel;
    return this;
  }

   /**
   * the fuel of the vehicle model
   * @return fuel
  **/
  @ApiModelProperty(required = true, value = "the fuel of the vehicle model")
  @NotNull


  public FuelEnum getFuel() {
    return fuel;
  }

  public void setFuel(FuelEnum fuel) {
    this.fuel = fuel;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VehicleModel vehicleModel = (VehicleModel) o;
    return Objects.equals(this.id, vehicleModel.id) &&
        Objects.equals(this.brand, vehicleModel.brand) &&
        Objects.equals(this.model, vehicleModel.model) &&
        Objects.equals(this.torNumber, vehicleModel.torNumber) &&
        Objects.equals(this.startConstructionDate, vehicleModel.startConstructionDate) &&
        Objects.equals(this.endConstructionDate, vehicleModel.endConstructionDate) &&
        Objects.equals(this.fuel, vehicleModel.fuel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, brand, model, torNumber, startConstructionDate, endConstructionDate, fuel);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VehicleModel {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    torNumber: ").append(toIndentedString(torNumber)).append("\n");
    sb.append("    startConstructionDate: ").append(toIndentedString(startConstructionDate)).append("\n");
    sb.append("    endConstructionDate: ").append(toIndentedString(endConstructionDate)).append("\n");
    sb.append("    fuel: ").append(toIndentedString(fuel)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

